// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import { Console } from 'console';
import * as vscode from 'vscode';
import {TTTreeNode, testdata } from './treedata';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "ttdialog" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('extension.helloWorld', () => {
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		vscode.window.showInformationMessage('Hello World!');
	});
	context.subscriptions.push(disposable);
	let cmd2 = vscode.commands.registerCommand('extension.ttShowWindow', () => {
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		vscode.window.showErrorMessage('再来一个 Hello World!');
	});
	context.subscriptions.push(cmd2);

	testTree(context);
	testCustomView(context);

	let cmdtree =vscode.commands.registerCommand('data.showme',(pickitem:TTTreeNode)=>
	{
		console.log("picked="+pickitem.name);
	});
	context.subscriptions.push(cmdtree);

	let outputchannel = vscode.window.createOutputChannel("mychannel");
	let cmdtree2 =vscode.commands.registerCommand('data2.refresh',()=>
	{
		outputchannel.show();
		outputchannel.appendLine("debuglog data2.refresh");
		console.debug("data2.refresh");
	});
	context.subscriptions.push(cmdtree2);

	let cmdtree3 =vscode.commands.registerCommand('data1.refresh',()=>
	{
		outputchannel.show();
		outputchannel.appendLine("debuglog data1.refresh");
		console.log("data1.refresh");
	});
	context.subscriptions.push(cmdtree3);
}

function testTree(context: vscode.ExtensionContext)
{	
	var tdata =new testdata();
	tdata.root.children.push(new TTTreeNode("label001"));
	var c01 =new TTTreeNode("label002");
	tdata.root.children.push(c01);
	tdata.root.children.push(new TTTreeNode("nihao"));
	c01.children.push(new TTTreeNode("subnode"));

	let tree1 = vscode.window.registerTreeDataProvider("data1",tdata);
	context.subscriptions.push(tree1);
}
function testCustomView(context: vscode.ExtensionContext)
{
	var tdata =new testdata();
	tdata.root.children.push(new TTTreeNode("label001"));
	var c01 =new TTTreeNode("label002");
	tdata.root.children.push(c01);
	tdata.root.children.push(new TTTreeNode("nihao"));
	c01.children.push(new TTTreeNode("subnode"));

	//let tree2 = vscode.window.registerTreeDataProvider("data2",tdata);
	//context.subscriptions.push(tree2);
	let view = vscode.window.createTreeView("data2",{treeDataProvider:tdata });
	context.subscriptions.push(view);

}
// this method is called when your extension is deactivated
export function deactivate() {}
