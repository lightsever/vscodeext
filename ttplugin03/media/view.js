"use strict";
function QUI_Span(txt, parent = null) {
    if (parent == null)
        parent = document.body;
    var span = document.createElement("span");
    span.textContent = txt;
    span.append(document.createElement("br"));
    parent.append(span);
    return span;
}
function QUI_Button(txt, _event, parent = null) {
    if (parent == null)
        parent = document.body;
    var btn = document.createElement("button");
    btn.textContent = txt;
    btn.onclick = _event;
    parent.append(btn);
    return btn;
}
var VsCodeApi;
function initVsCodeAPI() {
    if (!VsCodeApi)
        VsCodeApi = acquireVsCodeApi();
}
class state {
    constructor() {
        this.count = 0;
    }
}
let laststate;
function recvMsg(msg) {
    let type = typeof (msg.data);
    QUI_Span("==recv:" + type + "  " + msg.data);
    if (type == "object") {
        QUI_Span("obj=" + JSON.stringify(msg.data));
    }
}
window.onload = () => {
    window.onmessage = recvMsg; //从vscode 接收事件
    initVsCodeAPI();
    laststate = VsCodeApi.getState();
    if (!laststate)
        laststate = new state();
    QUI_Span("hello world.");
    QUI_Button("send msg1", () => {
        laststate.count++;
        VsCodeApi.setState(laststate);
        QUI_Span("send str:" + laststate.count);
        VsCodeApi.postMessage("hello world.");
    });
    QUI_Button("send msg2", () => {
        laststate.count++;
        VsCodeApi.setState(laststate);
        QUI_Span("send str:" + laststate.count);
        VsCodeApi.postMessage({ "hi1": 23, "data": "ddd" });
    });
};
//# sourceMappingURL=view.js.map