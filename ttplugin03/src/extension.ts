// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { TTTreeNode, testdata } from './treedata';
import * as path from 'path';
import { ttpaleditor } from './customeditor';
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext)
{

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "ttdialog" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('extension.helloWorld', () =>
	{
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		vscode.window.showInformationMessage('Hello World!');
	});
	context.subscriptions.push(disposable);
	let cmd2 = vscode.commands.registerCommand('extension.ttShowWindow', () =>
	{
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		vscode.window.showErrorMessage('再来一个 Hello World!');
	});
	context.subscriptions.push(cmd2);

	testTree(context);
	testCustomView(context);

	testWebView(context);

	testCustomEdit(context);
}

function testCustomEdit(context: vscode.ExtensionContext)
{
	vscode.window.registerCustomEditorProvider("ttedit.pal",new ttpaleditor(context.extensionUri));
}
function testTree(context: vscode.ExtensionContext)
{
	var tdata = new testdata();
	tdata.root.children.push(new TTTreeNode("label001"));
	var c01 = new TTTreeNode("label002");
	tdata.root.children.push(c01);
	tdata.root.children.push(new TTTreeNode("nihao"));
	c01.children.push(new TTTreeNode("subnode"));

	let tree1 = vscode.window.registerTreeDataProvider("data1", tdata);
	context.subscriptions.push(tree1);
}
function testCustomView(context: vscode.ExtensionContext)
{
	var tdata = new testdata();
	tdata.root.children.push(new TTTreeNode("label001"));
	var c01 = new TTTreeNode("label002");
	tdata.root.children.push(c01);
	tdata.root.children.push(new TTTreeNode("nihao"));
	c01.children.push(new TTTreeNode("subnode"));

	//let tree2 = vscode.window.registerTreeDataProvider("data2",tdata);
	//context.subscriptions.push(tree2);
	let view = vscode.window.createTreeView("data2", { treeDataProvider: tdata });
	context.subscriptions.push(view);

}

function GetInitHtmlWithRes(urigif: vscode.Uri, urijs: vscode.Uri)
{
	let inithtml =
		`
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Cat Coding</title>
	<script src="${urijs}"></script>
</head>
<body>
	<img src="${urigif}" width="300" />
</body>
</html>
`;
	return inithtml;
}

let panel: vscode.WebviewPanel | null;
function recvmsg(msg: any):void
{
	//接收来自webview的数据
	let type = typeof (msg);
	console.log("recv=" + type + "  " + JSON.stringify(msg));

	panel?.webview.postMessage({"hi":1,"b":5});
	return;
}
function testWebView(context: vscode.ExtensionContext)
{
	let cmdtree = vscode.commands.registerCommand('data.showme', (pickitem: TTTreeNode) =>
	{
		console.log("picked=" + pickitem.name);
		if (panel == null)
		{//没有panel 创造一个

			let urimedia = vscode.Uri.joinPath(context.extensionUri, 'media');
			console.log("urimedia = " + urimedia);
			let option: vscode.WebviewOptions = {
				enableScripts: true,//是否允许webview中执行js，那必须的啊
				localResourceRoots: [urimedia]//如果要允许访问本地文件，这里需要配置路径
			};

			panel = vscode.window.createWebviewPanel("ttwin", "hello tt", vscode.ViewColumn.One, option);

			//let uri="vscode-resource://media/dep.png";
			let urigif = panel.webview.asWebviewUri(vscode.Uri.joinPath(urimedia, 'giphy.gif'));
			let urijs = panel.webview.asWebviewUri(vscode.Uri.joinPath(urimedia, 'view.js'));
			console.log("urigif = " + urigif);
			console.log("urijs = " + urijs);
			//如果传string 记得要用uri.toString(); uri.path不是完整路径
			panel.webview.html = GetInitHtmlWithRes(urigif, urijs);

			panel.webview.onDidReceiveMessage(recvmsg, null,context.subscriptions);

			panel.onDidDispose((e) =>
			{
				panel = null;
			});
		}
		else
		{//有panel 就重新显示到前面来
			panel.reveal();
		}
	});
	context.subscriptions.push(cmdtree);

}
// this method is called when your extension is deactivated
export function deactivate() { }
